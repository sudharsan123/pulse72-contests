<?php
	$contents="";

	if(isset($_GET['logout'])){
		unset($_SESSION['username']);
		$_SESSION['logged_out']=true;
		header("Location:{$localPath}home/?logout");
		exit;
	}
	
	if(isset($_SESSION['username'])){
		header("Location:$localPath");
		exit;
	}
	if(isset($_POST['login'])){
		$_POST['username']=mysql_real_escape_string($_POST['username']);
		$_POST['password']=mysql_real_escape_string($_POST['password']);
		
		if(validateLogin($_POST['username'],$_POST['password'])){
			$_SESSION['username']=$_POST['username'];
			header("Location:$localPath");
			exit;
		}
		else{
			$contents="<div class=\"alert-error alert\">
  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
  <strong>Warning!</strong> Invalid Login Credentials Provided!
</div>
";
		}
	}
	$contents.=<<<EOT
<div class='row'>
<div class='span6'>
<form class="form-horizontal" method='post' action='./signin'>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" name='username' id="inputEmail" placeholder="Email" required>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Password</label>
    <div class="controls">
      <input type="password" id="inputPassword" name='password' placeholder="Password" required>
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <button type="submit" name='login' class="btn">Sign in</button>
    </div>
  </div>
  </div>
 <div class='span3'>
 	<a href='/oauth'><button class="btn btn-primary">Login with Facebook</button></a>
 </div>
</form>
EOT;

