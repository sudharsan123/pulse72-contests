<?php
	require("../inc/functions.php");
	session_start();
	
	if(!isset($_GET['id']) || !isset($_GET['mode']) || !isset($_SESSION['username'])){
		die("fail");
	}
	
	establishConnection();
	$id=parseInt($_GET['id']);
	if(!$id)	die("0");
	
	//Query to get the validity of the given ID
	$query="SELECT `timestamp`,`likes`,`rating`,`approved`
		FROM `posts` 
		WHERE `ID`='$id'
		LIMIT 1";
	$res=mysql_query($query);
	if(!mysql_num_rows($res))	die("0");
	$current=mysql_fetch_assoc($res);
	
	//Query to check if the user has voted already
	$query="SELECT `value`
		FROM `votes`
		WHERE `username`='{$_SESSION['username']}' AND `postsid`='$id'";
	$prevote=mysql_query($query) or die(mysql_error());
	$value=0;
	$voted=false;
	if(mysql_num_rows($prevote)){
		$t=(mysql_fetch_assoc($prevote));
		$value=$t['value'];
		$voted=true;
	}
	$uservote=0;
	if($_GET['mode']=="up"){
		$uservote=1;
		if($voted && $value==1){
			$upval=-1;
			$uservote=0;
		}
		else if($voted && $value==-1){
			$upval=1;
		}
		else{
			$upval=1;
		}
	}
	else if($_GET['mode']=="down"){
		$uservote=-1;
		if($voted && $value==1){
			$upval=-1;
		}
		else if($voted && $value==-1){
			$upval=0;
			$uservote=0;
		}
		else{
			$upval=0;
			$downval=1;
		}
	}
	else die("0");
	
	$upval += $current['likes'];
	if($upval<501)	$total    = $current['approved'] + $upval/20;
	else		$total	  = $current['approved'] + 25;
	
	$total=floor($total*100)/100;
	$timestamp	  = $current['timestamp'];
	
	$post_query="UPDATE `posts`
		     SET `likes`='$upval' , `rating`='$total' , `timestamp`='$timestamp'
		     WHERE `ID`='$id' ";
	if($voted)
		$vote_query="UPDATE `votes`
			     SET `value`='$uservote' 
			     WHERE `postsid`='$id' AND `username`='{$_SESSION['username']}' ";
	else
		$vote_query="INSERT INTO `votes`(`username`,`postsid`,`value`)
			     VALUES('{$_SESSION['username']}','$id','$uservote')";
	
	mysql_query($vote_query);
	mysql_query($post_query);
	
	die("$total");
?>
