<?php
 if(!defined("_PULSE_"))
     die("Access Denied");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Meme 'n Troll </title>
		<script src="<?=$templatePath?>js/jquery.js"></script>
		<script src="<?=$templatePath?>bootstrap/js/bootstrap.min.js"></script>
		<script>var localPath='<?=$localPath?>'</script>
		<script src="<?=$templatePath?>js/actions.js"></script>
	 	<link href="<?=$templatePath?>bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
	 	<link rel="icon" type="image/png" href="<?=$localPath?>images/favicon.png" />
	</head>
	<body>
		<div class="navbar navbar-inverse navbar-fixed-top">
		      <div class="navbar-inner" id='header'>
			<div class="container">
			  
			  <a class="brand" href="<?=$localPath?>home/">Pulse72+ Contests</a>
			  <div class="nav-collapse collapse">
			    <ul class="nav">
			    <?php
			    	$lis=Array("home"=>"Home","rules"=>"Rules","scoreboard"=>"Scoreboard");
			    	foreach($lis as $a=>$b){
			    		$active="";
			    		if($a=="$title")	$active="class='active'";
			    		echo "<li $active><a href='{$localPath}$a/'>$b</a></li>";
			    	}
			    ?>
			      <?php 
			      	if(!$logged_in)
			      	echo "<li style='float:right'><a href=\"{$localPath}signin\">Login</a></li>";
			      	else 
			      	echo "<li style='float:right'><a href=\"{$localPath}logout\">Logout</a></li>
			      	<li style='float:right'>
			      	<a href=\"{$localPath}upload\"><span class='icon-upload icon-white'></span>&nbsp;Upload</a>
			      	</li>";
			      	?>
			    </ul>
			  </div><!--/.nav-collapse -->
			</div>
		      </div>
	    	</div>
	    	<?php
	    	if(isset($home)){
		    	echo "
		    	<div class='navbar'>
		    	</div>
		    	<div class='container'>
		    		<div class='hero-unit' id='superhero'>
		    			<h2>Meme 'n Troll</h2>
		    			<p>Keep calm and meme on!</p>
		    		</div>
		    	</div>";
	    	}?>
	    	<div class="container">
	    	  <div class="row">
	    	  	<div class='span12' id='user-messages'>
	    	  		<?=$message?>
	    	  	</div>
	    	  </div>
		  <div class="row">
		    <div class="span3" id="side-stream">
		      <div class="span3" id='ads' alt='Ads'>
		      	<script type="text/javascript"><!--
			google_ad_client = "ca-pub-8589855409211734";
			/* Pulse Wide Skyscraper */
			google_ad_slot = "8524790088";
			google_ad_width = 160;
			google_ad_height = 600;
			//-->
			</script>
			<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
		      </div>
		    </div>
		    <div class="span9" id="post-stream">
		      <?=$contents?>
		    </div>
		  </div>
		<hr/>
		<footer style="text-align:center;height:50px;">
			&copy; Copyright Pulse72+ 2012. All Rights Reserved.
		</footer>
		</div>
	</body>
</html>
