<!DOCTYPE html>
<html>
	<head>
		<title>Pulse72+ Contests - </title>
		<script src="<?=$templatePath?>js/jquery.js"></script>
		<script src="<?=$templatePath?>bootstrap/js/bootstrap.min.js"></script>
		<script src="<?=$templatePath?>js/actions.js"></script>
	 	<link href="<?=$templatePath?>bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
	 	<link rel="icon" type="image/png" href="<?=$localPath?>images/favicon.png" />
	</head>
	<body>
		<div class='hero-unit' id='loginhero'>
			<div>
	    			<h2>Login</h2>
	    			<a href="<?=$localPath?>">Back to Site&rarr;</a>
	    		</div>
    		</div>
		<div class="container">
		<?=$contents?>
		</div>
	</body>
</html>
