<?php

	if(!defined("_PULSE_")){
		die("Access Denied");
	}
	if(!isset($_SESSION['username'])){
		$contents="<div class=\"alert-error alert\" style=\"margin:10px;\">
	  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
	  <strong>Warning!</strong> You must be logged in to view this page. Click <a href=\"$localPath/login\">here</a> to login!
	  </div>";
	}
	else{
		$query="SELECT * FROM `userupload` WHERE `username`='{$_SESSION['username']}'";
		$q=mysql_query($query);
		$current=0;
		if(!mysql_num_rows($q))	$current=1;
		else{
			$c=mysql_fetch_assoc($q);
			$current=$c['recent']+1;
		}

		if($current==6){
			//$current--;
			$contents="<div class=\"alert-success alert\"  style=\"margin:20px;\">
			<h5> You have successfully completed the 5 rounds.. Bonus round coming soon :D<br/> 
			You can modify your previous submission if you want to.</h5>
			</div>";
		}
		if(isset($_POST['upload'])){
			$filename=isValidFile($_POST['title']);
			$path="images/".md5($filename.time()).".jpg";
			if($_SESSION['captcha']!=md5($_POST['captcha'])){
				$contents.="<div class=\"alert-error alert\"  style=\"margin:10px;\">
				  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
				  <strong>Oops!</strong> You gave the captcha wrong!<br/> Click <a href=\"{$localPath}upload\">here					</a> to try again!
				</div>";
			}
			else{
				unset($_SESSION['captcha']);
				if(uploadFile($_FILES['input-file'],Array("image/jpeg"),1*1024*1024,$path)
					 && parseInt($_POST['round'])<=6 && parseInt($_POST['round'])>0 ){
					$contents.="<div class=\"alert-success alert\"  style=\"margin:10px;\">
					  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
					  <strong>Well Done!</strong> Your image was submitted successfully!
					</div>";
					if(parseInt($_POST['round'])==$current){
						$data['path']=$path;
						$data['posted_by']=$_SESSION['username'];
						$data['round']=$current;
						$data['title']=mysql_real_escape_string($_POST['title']);
						$id=saveData("posts",$data);
						$data2['username']=$_SESSION['username'];
						$data2['recent']=$current;
						savedata("userupload",$data2);
						$shareURL="https://www.facebook.com/dialog/feed?
app_id=302874733128390&
link=http://{$_SERVER['SERVER_NAME']}{$localPath}view/{$id}&
picture=http://www.pulse72plus.com/template/images/logo.png&
name=Pulse72plus-%20Meme%20n%20Troll&
caption=Keep%20calm%20and%20meme%20on!&
description=The%20Online%20Rage%20Comic%20Contest&
redirect_uri=http://{$_SERVER['SERVER_NAME']}{$localPath}view/{$id}";
						$contents.="<div class=\"alert-info alert\"  style=\"margin:10px;\">
						  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
						  <strong></strong> Click <a href=\"{$localPath}view/$id\">here</a> to view your upload!
						</div>
						<div class=\" alert\"  style=\"margin:10px;\">
							<a href='$shareURL' target='_blank'>
							<img src='{$localPath}images/share.gif' alt='Share on Facebook'/>
							</a>
						</div>
						";
					}
					else{
						$data['path']=$path;
						$data['posted_by']=$_SESSION['username'];
						$data['round']=parseInt($_POST['round']);
						$data['title']=mysql_real_escape_string($_POST['title']);
						$query="UPDATE `posts` 
							SET `path`='{$data['path']}' , `title`='{$data['title']}' , `likes`='0',
								`approved`='0' , `rating`='0' 
							WHERE `posted_by`='{$data['posted_by']}' AND `round`='{$data['round']}';";
						mysql_query($query);
						$title=htmlentities($data['title']);
						$query="SELECT `ID` FROM `posts` 
							WHERE `posted_by`='{$data['posted_by']}' AND `round`='{$data['round']}';";
						$data=mysql_fetch_assoc(mysql_query($query));
						
						$query="DELETE 
							FROM `votes` 
							WHERE `postsid` = '{$data['ID']}'";
						mysql_query($query);
						$id=$data['ID'];
						$shareURL="https://www.facebook.com/dialog/feed?
app_id=302874733128390&
link=http://{$_SERVER['SERVER_NAME']}{$localPath}view/{$id}&
picture=http://www.pulse72plus.com/template/images/logo.png&
name={$title}&
caption=Submitted%20a%20meme%20for%20Meme%20n%20Troll&
description=The%20Online%20Rage%20Comic%20Contest&
redirect_uri=http://{$_SERVER['SERVER_NAME']}{$localPath}view/{$id}";
						$contents.="<div class=\"alert-info alert\"  style=\"margin:10px;\">
						  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
						  <strong></strong> Your post was successfully edited!
						</div>
						
						<div class=\" alert\"  style=\"margin:10px;\">
							<a href='$shareURL' target='_blank'>
							<img src='{$localPath}images/fb.gif' alt='Share on Facebook'/>
							</a>
						</div>
						";
					}
				}
				else{
					$contents="<div class=\"alert-error alert\"  style=\"margin:10px;\">
					  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">x</button>
					  <strong>Oops!</strong> Your image wasn't uploaded!<br/> Click 
					  <a href=\"{$localPath}upload\">here</a> to try again!
					</div>";
				}
			}
		}
		else{
			if($current==6)	$current--;
			$contents.=<<<EOT
	<div class="span8" style="padding:10px;">
		<div>
			<h3>Submit your entry</h3>
		</div>
		<div class='alert alert-info' style='width:600px'>
		You can now submit for Round #$current or edit any of your previous submissions.<br/>
		You might wanna read the <a href='{$localPath}rules/'>rules</a> first.
		</div><br/>
		<form class="form-horizontal" method="post" action="{$localPath}upload" enctype='multipart/form-data'>
			<div class="control-group">
			    <label class="control-label" for="inputFile">File</label>
			    <div class="controls">
			      <input type="file" id="inputFile" placeholder="File" required name='input-file'><br/>
			      <span class="help-inline">Only JPEG images. Maximum size 1MB</span>
			    </div>
			</div>
			<div class="control-group">
			    <label class="control-label" for="title">Post Title</label>
			    <div class="controls">
			 <input type="text" class="input-xlarge" id="title" placeholder="Post title" name='title' maxlength=100 required>
			    </div>
			</div>
			<div class="control-group">
			    <label class="control-label" for="round">Round #</label>
			    <div class="controls">
			    <input type="number" class="input-large" name='round' id="round" value='$current' min='1' max='$current' required>
			    <span class="help-inline">Change this field only if you want to edit your previous entry</span>
			    </div>
			</div>
			<div class="control-group">
			    <label class="control-label" for="captcha">Image Verification</label>
			    <div class="controls">
			      <img src="{$localPath}inc/captcha.php" /><br/>
			      <input type="text" class="input-large" id="captcha" placeholder="Captcha" name='captcha' required><br/>
			      <span class="help-inline">Enter the given text</span>
			    </div>
			</div>
			<div class="control-group">
			    <div class="controls">
				  <button class="btn btn-primary btn-large" type="submit" name='upload'>Post Entry</button>
			    </div>
			</div>
		</form>
		</div>
EOT;
		}		
	}
	
