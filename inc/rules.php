<?php
$contents=<<<EOT
<div style='padding:25px'>
<h2>Rules</h2>
<ul>
<li>All trolls and memes which are submitted must be original. Plagiarism will be penalized.</li>
<li>There are totally 6 rounds in this contest - 5 normal rounds + 1 bonus round. Each round carries same weightage of points.</li>
<li>You can submit an entry for a particular round only if you have submitted entries for all the previous rounds. For example, to submit an entry for round 3, you must complete round 1 and round 2 first. Hence, you must complete all rounds in order to attempt the bonus round.</li>
<li>The rules for individual rounds are given below. For each round, there is a Guidelines and Examples link which will help you complete the round.</li>
</ul>
<br/>
<h3>Round #1</h3>
Ok let's get started with something easy!<br/>
Make a meme with the "Success Kid" character given below:<br/>
<img src='{$localPath}images/success-kid.jpg' alt='Success Kid'/>
<br/><br/>
<h3>Round #2</h3>
How easy was it to write a couple of lines? Let's step it up! Oh! This time you have got to choose your own picture!<br/>
Make a meme that starts with the line "That Awkward Moment When"<br/>
<br/><br/>
<h3>Round #3</h3>
So why stick to just one picture. Let's make it more!<br/>
Make a meme with "How People See"<br/>
<br/><br/>
<h3>Round #4</h3>
Got pissed with making memes?? Here's to chance to show your anger.<br/>
Make a troll comic with the Rage Face!<br/>
<img src='{$localPath}images/rageface.jpg' alt='Rage Guy'/>
<br/><br/>
<h3>Round #5</h3>
Now that you've made it to the last round, it's time to test a different skill!<br/>
Make your own troll face!<br/>
Use Photoshop, Paint whatever - just make a hilarious troll face!<br/>
<br/><br/>
</div>
EOT;

