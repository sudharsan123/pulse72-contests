<?php
	$page=parseInt($_GET['no'])-1;
	$p=$page*10;
	$query="SELECT * 
		FROM `posts` 
		ORDER BY `ID` DESC 
		LIMIT $p,10";
	establishConnection();
	$res=mysql_query($query);
	$numrows=mysql_num_rows($res);
	if(!$numrows){
	$contents="<div class='span8 label label-inverse' style='margin:25px;font-size:1.1em;line-height:25px;padding:10px;'>
				Nothing to display<br/>Click <a href='{$localPath}home'>here</a> to get back..</div>";
	}
	else{
		$contents="<div>";
		$entries=0;
		if($admin){
			$contents.="
			<script>
			$(document).ready(function(){
				$('.rate-button').click(function(){
					var btn=this;
					var clicked=this.getAttribute('data-id');
					var score=$('#score-'+clicked)[0].value;
					$.ajax({
						url: localPath+'ajax/adminrate.php',
						data: { mode: 'up', id: clicked , score: score },
						success: function(data) {
							if(data!='0' && data!='fail'){
								if(!$(btn).hasClass('btn-success'))
						    			$(btn).addClass('btn-success');
						    			
						    		$('.likes-'+clicked).html(data);
						    	}
						    	else if(data=='0'){
						    		$('.likes-'+clicked).html('0');
						    		$(btn).removeClass('btn-success');
						    	}
						    	else if(data=='fail'){
						    		window.location.href='/contests/signin/';
						    	}
						}
					});
				});
			});
			</script>";
		}
		while($row=mysql_fetch_assoc($res)){
			$entries++;
			$vote;
			$likedisabled="";
			if(!isset($_SESSION['username'])){
				$vote=0;
				$likedisabled="disabled";
			}
			else
			{
			$query="SELECT `value` FROM `votes` WHERE `username`='{$_SESSION['username']}' AND `postsid`='{$row['ID']}'";
			$vote=0;
			$t=mysql_query($query);
				if(mysql_num_rows($t)){
					$t=mysql_fetch_assoc($t);
					$vote=$t['value'];
				}
			}
			$btnclass1=$btnclass2="";
			if($vote==1)		$btnclass1="btn-success";
			$txt="";
			if($row['approved'])	$txt="&nbsp;&nbsp;<span class='badge badge-success'>
				<span class='icon-ok icon-white' title='Voted by the Judge'></span></span>";
			$contents.="<div class='row' style='margin-bottom:25px;margin-top:25px;padding:20px;'>";
			$contents.="<div class='span6'><a href=\"{$localPath}view/{$row['ID']}\" target='_blank'><img src='$localPath".$row['path']."' style='width:100%' alt='{$row['title']}'/></a></div>";
			$contents.="<div class='span2' style='width:170px;'><h4 style='word-wrap:break-word'>
				<a href=\"{$localPath}view/{$row['ID']}\">".$row['title']."</a>{$txt}</h4>
				<h5><a href=\"{$localPath}users/{$row['posted_by']}\">".$row['posted_by']."</a></h5>";
			if($admin){
				$contents.="
				<input type='number' id='score-{$row['ID']}' name='score' value='{$row['approved']}' min='0' max='75' style='width:45px'/>
				<br/>
				<div class=\"btn-group\">
				  <button class=\"btn rate-button {$btnclass1} {$likedisabled}\" data-id='{$row['ID']}' style='width:175px;'>
					  <span class=\"icon-thumbs-up\"></span>Like&nbsp;&nbsp;
				  </button>
				</div><br/><br/>
				Rating: <span class='label label-inverse likes-{$row['ID']}'>{$row['rating']}</span>
				<br/>
				<div class='code' style='padding:5px;opacity:0.6;'>
				<small>
				Uploaded for Round#{$row['round']}<br/>
				<span class='icon-time'></span>&nbsp;{$row['timestamp']}<br/>
				</small>
				</div>
				</div>
				";
			}
			else{
			$contents.="
				<div class=\"btn-group\">
				  <button class=\"btn like-button {$btnclass1} {$likedisabled}\" data-id='{$row['ID']}' style='width:175px;'>
					  <span class=\"icon-thumbs-up\"></span>Like&nbsp;&nbsp;
				  </button>
				</div><br/><br/>
				Rating: <span class='label label-inverse likes-{$row['ID']}'>{$row['rating']}</span>
				<br/>
				<div class='code' style='padding:5px;opacity:0.6;'>
				<small>
				Uploaded for Round#{$row['round']}<br/>
				<span class='icon-time'></span>&nbsp;{$row['timestamp']}<br/>
				</small>
				</div>
			</div>";
			}
			$contents.="</div><hr class='separator' style='margin-left:24px;'>";
		}
		$dis="";
		$nxtp=$prevp=$localPath."home/";
		$nxtp.=($page+2);
		$prevp.=($page);
		if($page==0){
			$dis="disabled";
		}
		if($numrows>10)
		$contents.=<<<EOT
		<ul class='pager'>
		    <li class="{$dis}"><a href="{$prevp}">Prev</a></li>
		    <li class="active"><a href="{$nxtp}">Next</a></li>
		</ul>
EOT;
		$contents.="</div>";
	}
