<?php
	define("_PULSE_","Pulse Constant");
	require("./inc/functions.php");
	session_start();
	establishConnection();
	$templatePath="/template/";
	$localPath="/";
	$message="";
	if(isset($_SESSION['logged_out'])){
		$message="<div class='label-success label' style='width:915px;margin:0 0 25px 0;font-size:1.1em;line-height:25px;padding:10px;'>
			You have been logged out!</div>";
		unset($_SESSION['logged_out']);
		unset($_SESSION['username']);
	}
	$_SESSION['username']="admin";
	//unset($_SESSION['username']);
	$admin=false;
	if($_SESSION['username']=="admin")	$admin=true;
	$logged_in=false;
	if(isset($_SESSION['username']))	$logged_in=true;
	$contents="";
	$title="";
	if(!isset($_GET['page'])){
		header("Location:{$localPath}home/");
		exit;
	}
	$page=$_GET['page'];
	if($page!="upload" && $page!="rules"){
		$home=1;
	}
	if($page=="upload"){
		include("./inc/upload.php");
	}
	else if($page=="users"){
		include("./inc/users.php");
	}
	else if($page=="vote"){
		include("./inc/hot.php");
		$title="home";
	}
	else if($page=="view"){
		include("./inc/view.php");
	}
	else if($page=="login"){
		include("./login.php");
		include("./template/login.php");
		exit;
	}
	else if($page=="logout"){
		$_GET['logout']=true;
		include("./login.php");
	}
	else if($page=="rules"){
		include("./inc/rules.php");
		$title="rules";
	}
	else if($page=="score"){
		include("./inc/scoreboard.php");
		$title="scoreboard";
	}
	include("./template/index.php");
	
?>
