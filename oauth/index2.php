<?php
require_once 'openid.php';
$openid = new LightOpenID("http://localhost");
 
$openid->identity = 'https://www.google.com/accounts/o8/id';
$openid->required = array(
  'namePerson/first',
  'namePerson/last',
  'contact/email',
);
$openid->returnUrl = 'http://localhost/newsite/oauth/login-google.php';
header("Location: ".$openid->authUrl());
?>
 